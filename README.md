# QtService CMake Qt5 fork
Cross-platform C++ background service/daemon package using the Qt framework

## Documentation
Check out [Nymea's](https://developers.guh.io/qtservice.html) pages.

## Usage
1. Clone this repo into your project.
2. In your CMakeLists.txt, call `add_subdirectory(LOCATION_OF_CLONED_REPO)`.
3. Also call  `target_link_libraries(YOUR_TARGET PUBLIC qtservice)`.

## License
BSD-style licence (see source files for more details)